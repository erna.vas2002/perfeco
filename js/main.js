// BURGER HEADER 
const burgerBtn = document.querySelector('.nav__btn');
const navList = document.querySelector('.nav__list');

burgerBtn.addEventListener('click', (e) => {
  navList.classList.toggle('nav--open');
  console.log(e.tarhet)
})

// ACCORDION 
const questions = document.querySelectorAll('.accordion__question');
const answers = document.querySelectorAll('.accordion__answer');

questions.forEach(question => {
  question.addEventListener('click', () => {
    const activeContent = document.querySelector('#' + question.dataset.id);
    console.log(activeContent)

    activeContent.classList.contains('active-tab') ?  activeContent.classList.remove('active-tab') : activeContent.classList.add('active-tab');
  })
})